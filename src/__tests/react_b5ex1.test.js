import { configure, mount } from "enzyme";
import "@testing-library/jest-dom";
import App from "../App.js";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";

configure({ adapter: new Adapter() });

const app = mount(<App />);

describe("b5ex01: Dynamic Filtering", () => {
  it("In a functional App.js there should be a form with an input (with an onChange event updating state) and a button", () => {
    const form = app.find("form");
    const input = app.find("input");
    const button = app.find("button");

    let allGood = false;

    input.prop("onChange") !== undefined && (allGood = true);

    if (!allGood || !form || !button) return false;
  });
  it("initially we should see all the products: 2 shirts and 1 hat", () => {
    const shirts = app.find({ children: "t-shirts" });
    const hats = app.find({ children: "hats" });

    if (shirts.length !== 2 || hats.length !== 1) return false;
  });

  it("By typing 'shirt' we should see two products", () => {
    app.find("input").simulate("change", { target: { value: "shirt" } });

    const shirts = app.find({ children: "t-shirts" });
    const hats = app.find({ children: "hats" });

    if (shirts.length < 1 || hats.length !== 0) return false;
  });

  it("By typing 'all' we should see all the products", () => {
    const input = app.find("input");
    input.simulate("change", { target: { value: "all" } });

    const shirts = app.find({ children: "t-shirts" });
    const hats = app.find({ children: "hats" });

    if (shirts.length < 1 || hats.length < 1) return false;
  });

  it("By typing 'asdf' we should see no products and render 'No products found' phrase", () => {
    const input = app.find("input");
    input.simulate("change", { target: { value: "asdf" } });

    const shirts = app.find({ children: "t-shirts" });
    const hats = app.find({ children: "hats" });

    const notFound = app.find({ children: "No products found" });

    if (shirts.length !== 0 || hats.length !== 0 || !notFound) return false;
  });
});
