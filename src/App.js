import logo from './logo.svg';
import './App.css';
import data from './data';
import { useState } from 'react';

function App() {
  const [search,setSearch] = useState("")
  const handleChange = event => {
    let value = event.target.value;
    setSearch(value);
   }
  const renderItems = (data) => {
    return data.map((item, i) => {
      let { category } = item
      let { product } = item
      let { price } = item
      const ifs = (search,category) => {
        if (search==category) {
          var found=true
          return found
        }
        else if (search=="all"||search=="") {
          var found=true
          return found
        }
        else {
          var found=false
        }
      }
      return (
        (ifs(search,category)==true) ? <div>
          <h2>{product}</h2><p >{category}</p><p>{price}</p>
          </div> : null
      )
    })
  }
  return <div className="App">
      <h1>Type name of category</h1>
      <input onChange={handleChange} />
      {renderItems(data)}
      </div>
}

export default App;
